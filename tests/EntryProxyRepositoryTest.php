<?php

/** @noinspection PhpUnhandledExceptionInspection */

namespace Zalmoksis\Dictionary\Storage\Tests;

use PHPUnit\Framework\{MockObject\MockObject, TestCase};
use Zalmoksis\Dictionary\Model\{Entry, Headword, Translation};
use Zalmoksis\Dictionary\Model\Collections\{Headwords, Translations};
use Zalmoksis\Dictionary\Storage\{EntryProxyRepository, EntryRepository, EntriesIndexedById};

final class EntryProxyRepositoryTest extends TestCase {
    protected EntryProxyRepository $proxyRepository;

    /** @var EntryRepository | MockObject */
    protected EntryRepository $innerRepository;

    function setUp(): void {
        $this->proxyRepository = new EntryProxyRepository(
            $this->innerRepository = $this->createMock(EntryRepository::class)
        );
    }

    function testInterface(): void {
        static::assertInstanceOf(EntryRepository::class, $this->proxyRepository);
    }

    function testSaving(): void {
        $entry1 = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 1')))
            ->setTranslations(new Translations(new Translation('translation 1')));

        $this->innerRepository->expects(static::exactly(1))->method('save')
            ->with(clone $entry1)
            ->willReturn('id 1');

        static::assertEquals('id 1', $this->proxyRepository->save(clone $entry1));
    }

    // TODO: Deep cloning!
    function testFindingEntryById(): void {
        $entry1 = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 1')))
            ->setTranslations(new Translations(new Translation('translation 1')));
        $entry2 = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 2')))
            ->setTranslations(new Translations(new Translation('translation 2')));

        $this->innerRepository->expects(static::exactly(2))->method('findById')
            ->withConsecutive(['id 1'], ['id 2'])
            ->willReturnOnConsecutiveCalls(clone $entry1, clone $entry2);

        $this->assertEquals(clone $entry1, $this->proxyRepository->findById('id 1'));
        $this->assertEquals(clone $entry2, $this->proxyRepository->findById('id 2'));
        $this->assertEquals(clone $entry1, $this->proxyRepository->findById('id 1'));
    }

    function testFindingEntryByIdWhenNothingFound(): void {
        $this->innerRepository->expects(static::once())->method('findById')
            ->with('id 1')
            ->willReturn(null);

        $this->assertEquals(null, $this->proxyRepository->findById('id 1'));
        $this->assertEquals(null, $this->proxyRepository->findById('id 1'));
    }

    function testFindingCachedEntry(): void {
        $entry1 = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 1')))
            ->setTranslations(new Translations(new Translation('translation 1')));

        $this->innerRepository->expects(static::once())->method('save')
            ->with(clone $entry1)
            ->willReturn('id 1');
        $this->innerRepository->expects(static::never())->method('findById');

        $this->proxyRepository->save(clone $entry1);
        $this->assertEquals(clone $entry1, $this->proxyRepository->findById('id 1'));
    }

    function testFindingEntriesByHeadword(): void {
        $entry5 = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 5')))
            ->setTranslations(new Translations(new Translation('translation 5')));
        $entry6 = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 6')))
            ->setTranslations(new Translations(new Translation('translation 6')));

        $this->innerRepository->expects(self::once())->method('findByHeadword')
            ->with('headword')
            ->willReturn(new EntriesIndexedById([
                'id 5' => clone $entry5,
                'id 6' => clone $entry6,
            ]));
        $this->innerRepository->expects(self::never())->method('findById');

        $this->assertEquals(new EntriesIndexedById([
            'id 5' => clone $entry5,
            'id 6' => clone $entry6,
        ]), $this->proxyRepository->findByHeadword('headword'));

        $this->assertEquals($entry5, $this->proxyRepository->findById('id 5'));
        $this->assertEquals($entry6, $this->proxyRepository->findById('id 6'));
    }

    function testFindingAllHeadwords(): void {
        $this->innerRepository->expects(self::once())->method('findHeadwords')
            ->with(0, 1)
            ->willReturn(new Headwords(new Headword('headword 1'), new Headword('headword 2')));

        $this->assertEquals(
            new Headwords(new Headword('headword 1'), new Headword('headword 2')),
            $this->proxyRepository->findHeadwords()
        );
    }

    function testFindingLimitedNumberOfHeadwords(): void {
        $this->innerRepository->expects(self::once())->method('findHeadwords')
            ->with(3, 5)
            ->willReturn(new Headwords(new Headword('headword 1'), new Headword('headword 2')));

        $this->assertEquals(
            new Headwords(new Headword('headword 1'), new Headword('headword 2')),
            $this->proxyRepository->findHeadwords(3, 5)
        );
    }

    function testDroppingById(): void {
        $entry = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 1')));

        $this->innerRepository->expects(self::once())->method('save')->with(clone $entry)->willReturn('id 1');
        $this->innerRepository->expects(self::once())->method('deleteById')->with('headword 1');
        $this->innerRepository->expects(self::never())->method('findById');

        $this->proxyRepository->save(clone $entry);
        $this->proxyRepository->deleteById('headword 1');
        $this->assertNull($this->proxyRepository->findById('headword 1'));
    }

    function testDroppingByIdWhenNotSaved(): void {
        $this->innerRepository->expects(self::once())->method('deleteById')->with('headword 1');
        $this->innerRepository->expects(self::never())->method('findById');

        $this->proxyRepository->deleteById('headword 1');
        $this->assertNull($this->proxyRepository->findById('headword 1'));
    }

    function testDroppingAll(): void {
        $entry = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 1')));

        $this->innerRepository->expects(self::once())->method('save')->with(clone $entry)->willReturn('id 1');
        $this->innerRepository->expects(self::once())->method('drop');
        $this->innerRepository->expects(self::never())->method('findById');

        $this->proxyRepository->save(clone $entry);
        $this->proxyRepository->drop();
        $this->assertNull($this->proxyRepository->findById('id 1'));
    }

    function testDroppingAllWhenNotSaved(): void {
        $entry = (new Entry())
            ->setHeadwords(new Headwords(new Headword('headword 1')));

        $this->innerRepository->expects(self::once())->method('save')->with(clone $entry)->willReturn('id 1');
        $this->innerRepository->expects(self::once())->method('drop');
        $this->innerRepository->expects(self::never())->method('findById');

        $this->proxyRepository->save(clone $entry);
        $this->proxyRepository->drop();
        $this->assertNull($this->proxyRepository->findById('id 1'));
    }
}
