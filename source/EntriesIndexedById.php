<?php

namespace Zalmoksis\Dictionary\Storage;

use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\Entry;

class EntriesIndexedById extends Collection {
    function __construct(array $entries = []) {
        $this->elements = $entries;
    }

    function add(string $entryId, Entry $entry): self {
        $this->elements[$entryId] = $entry;

        return $this;
    }
}
