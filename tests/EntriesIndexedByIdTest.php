<?php

namespace Zalmoksis\Dictionary\Storage\Tests;

use Countable;
use Iterator;
use PHPUnit\Framework\TestCase;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Entry, Headword};
use Zalmoksis\Dictionary\Model\Collections\Headwords;
use Zalmoksis\Dictionary\Storage\EntriesIndexedById;

final class EntriesIndexedByIdTest extends TestCase {
    private EntriesIndexedById $entriesIndexedById;

    protected function setUp(): void {
        $this->entriesIndexedById = new EntriesIndexedById([
            'id 1' => (new Entry()),
            'id 2' => (new Entry()),
        ]);
    }

    function testIfExtendsCollection() {
        self::assertInstanceOf(Collection::class, new EntriesIndexedById());
    }

    function testIfImplementsCountable() {
        $this->assertInstanceOf(Countable::class, new EntriesIndexedById());
    }

    function testIfImplementsIterator() {
        $this->assertInstanceOf(Iterator::class, new EntriesIndexedById());
    }

    function testAdding() {
        $entriesIndexedById = (new EntriesIndexedById([
            'id 1' => (new Entry())->setHeadwords(new Headwords(new Headword('headword 1'))),
            'id 2' => (new Entry())->setHeadwords(new Headwords(new Headword('headword 2'))),
        ]))->add('id 3', (new Entry())->setHeadwords(new Headwords(new Headword('headword 3'))));

        self::assertCount(3, $entriesIndexedById);

        foreach ($entriesIndexedById as $index => $element) {
            $this->assertEquals([
                'id 1' => (new Entry())->setHeadwords(new Headwords(new Headword('headword 1'))),
                'id 2' => (new Entry())->setHeadwords(new Headwords(new Headword('headword 2'))),
                'id 3' => (new Entry())->setHeadwords(new Headwords(new Headword('headword 3'))),
            ][$index], $element);
        }
    }
}
