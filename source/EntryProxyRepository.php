<?php

namespace Zalmoksis\Dictionary\Storage;

use Iterator;
use Zalmoksis\Dictionary\Model\{Collections\Headwords, Entry};

final class EntryProxyRepository implements EntryRepository {
    private EntryRepository $storage;
    private array $entries = [];

    function __construct(EntryRepository $storage) {
        $this->storage = $storage;
    }

    function save(Entry $entry): string {
        $id = $this->storage->save($entry);
        $this->entries[$id] = $entry;

        return $id;
    }

    function findById(string $id): ?Entry {
        return array_key_exists($id, $this->entries) // to store null results when entry not found
            ? $this->entries[$id]
            : $this->entries[$id] = $this->storage->findById($id);
    }

    function findByHeadword(string $headword): EntriesIndexedById {
        $entriesIndexedById = $this->storage->findByHeadword($headword);

        foreach ($entriesIndexedById as $id => $entry) {
            $this->entries[$id] = $entry;
        }

        return $entriesIndexedById;
    }

    function findHeadwords(int $limit = 0, int $page = 1): Headwords {
        return $this->storage->findHeadwords($limit, $page);
    }

    function getIterator(): Iterator {
        return $this->storage->getIterator();
    }

    function deleteById(string $entryId): void {
        $this->storage->deleteById($entryId);
        $this->entries[$entryId] = null;
    }

    function drop(): void {
        $this->storage->drop();

        foreach ($this->entries as $entryId => $entry) {
            $this->entries[$entryId] = null;
        }
    }
}
