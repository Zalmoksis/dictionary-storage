<?php

namespace Zalmoksis\Dictionary\Storage;

use Zalmoksis\Dictionary\Model\{Collections\Headwords, Entry};
use Iterator;

interface EntryRepository {
    function save(Entry $entry): string;
    function findById(string $entryId): ?Entry;
    function findByHeadword(string $headword): EntriesIndexedById;
    function findHeadwords(int $limit = 0, int $page = 1): Headwords;
    function getIterator(): Iterator;
    function deleteById(string $entryId): void;
    function drop(): void;
}
