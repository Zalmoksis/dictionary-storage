# Changelog

All notable changes to this project will be documented in this file
in accordance with [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

The project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.1] — 2020-03-01
## Changed
- Version range update of `zalmoksis/dictionary-model` to include `0.19`

## [0.8.0] — 2020-01-11
## Added
- `DictionaryStorageException`
## Changed
- `EntryRepository::dropById` to `EntryRepository::deleteById`
- `EntryRepository::dropAll` to `EntryRepository::drop`

## [0.7.0] — 2020-01-11
## Added
- `EntryRepository::dropById`
## Changed
- `DictionaryStorage` renamed to `EntryRepository`
- All methods renamed to remove the word Entry.
- `DictionaryProxyStorage` renamed to `EntryProxyRepository`.

## [0.6.0] — 2020-01-03
## Added
- `DictionaryStorage::iterateOverEntry` and `DictionaryStorage::dropEntries`;
  implementation in `DictionaryProxyStorage`

## [0.5.2] — 2019-12-20
### Changed
- Version range update: `zalmoksis\dictionary-model` to 0.18

## [0.5.1] — 2019-12-11
### Changed
- Version range update: `zalmoksis\dictionary-model` to 0.17

## [0.5.0] — 2019-12-03
### Added
- `DictionaryStorage::findHeadwords`

## [0.4.0] — 2019-12-01
### Changed
- `DictionaryProxyStorage` is now `final` and its `protected` properties become `private`

## [0.3.0] — 2019-11-30
### Added
- Gitlab CI integration including coding standard check
- Missing unit tests
### Changed
- PHP upgrade to 7.4
- Strong typing of properties
- `EntriesIndexedById` implements `Collection`
- PHP unit and Composer configuration update
### Removed
- Accidental code

## [0.2.0] — 2019-02-24
### Changed
- `Storage::findEntriesByHeadword` now is expected to return a collection indexed by entry ids.

## [0.1.4] — 2019-02-24
### Changed
- Model version range was increased to include 0.13.* and 0.14.*

## [0.1.3] — 2019-02-08
### Added
- More unit tests
### Changed
- PHPUnit was upgraded to 8.0.

## [0.1.2] — 2019-02-06
### Changed
- Model version range was increased to include 0.12.*

## [0.1.1] — 2019-01-29
### Fixed
- Fixed missing support for null result of `DictionaryStorage::findEntryById`
- `DictionaryProxyStorage` now correctly caches null results.

## [0.1.0] — 2019-01-29
### Added
- Initial `DictionaryStorage` interface
- Initial `DictionaryProxyStorage` implementation
